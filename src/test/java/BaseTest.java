import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import helper.ElementHelper;
import helper.StoreHelper;
import model.ElementInfo;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

    public class BaseTest {


        String url = "https://www.trendyol.com/";
        @BeforeScenario
        public void senaryoOncesi() throws InterruptedException {
            System.out.println("-----Senaryo başlangıcı----");
            System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriverty.exe");
            driver = new ChromeDriver();
            action = new Actions(driver);

            driver.manage().window().maximize();
            System.out.println("driver calisti");

        }
        static WebDriver driver;
        static Actions action;

        @AfterScenario
        public void senaryoSonrasi() {
            driver.quit();
            System.out.println("-----Senaryo sonu----");
        }
        private WebElement findElement(String key) {
            ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
            By infoParam = ElementHelper.getElementInfoToBy(elementInfo);
            WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
            WebElement webElement = webDriverWait
                    .until(ExpectedConditions.presenceOfElementLocated(infoParam));
            ((JavascriptExecutor) driver).executeScript(
                    "arguments[0].scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'})",
                    webElement);
            return webElement;
        }
    }

