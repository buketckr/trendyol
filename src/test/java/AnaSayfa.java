import com.thoughtworks.gauge.Step;
import helper.ElementHelper;
import helper.StoreHelper;
import model.ElementInfo;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AnaSayfa extends BaseTest {


    @Step("Anasayfaya git")
    public void implementation1() throws InterruptedException {
        driver.get(url);
        System.out.println("Anasayfa açıldı");
        Thread.sleep(4000);
    }

    @Step("4 saniye bekle")
    public void bekleme() throws InterruptedException {
        Thread.sleep(4000);

    }
}